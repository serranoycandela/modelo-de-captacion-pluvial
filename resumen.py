from os import listdir
from os.path import join, isdir
import numpy as np
import csv

scenariosFolder = "Users/fidel/Dropbox/EscenariosMarzo"

matrizR = []
matrizR.append(["escenario", "cat1", "cat2", "cat3", "cat4", "cat5"])
maximo = 0
minimo = 200
for carpeta in sorted(listdir(scenariosFolder)):
    carpetaPath = join(scenariosFolder, carpeta)
    if isdir(carpetaPath) and carpeta.startswith("escenario"):
        for shapefile in listdir(carpetaPath):
            
            if shapefile.endswith(".shp"):
                print carpeta, shapefile
                shape_path = join(carpetaPath, shapefile)
                manzana_layer = QgsVectorLayer(shape_path, "manzanas", "ogr")
                frecM = [carpeta,0,0,0,0,0]
                intervalo = 17.6
                for manzana in manzana_layer.getFeatures():
                    if  manzana['p_lluvia_i'] < intervalo:
                        frecM[1] += 1
                    if  manzana['p_lluvia_i'] < intervalo * 2.0 and  manzana['p_lluvia_i'] > intervalo:
                        frecM[2] += 1
                    if  manzana['p_lluvia_i'] < intervalo * 3.0 and  manzana['p_lluvia_i'] > intervalo * 2.0:
                        frecM[3] += 1
                    if  manzana['p_lluvia_i'] < intervalo * 4.0 and  manzana['p_lluvia_i'] > intervalo * 3.0:
                        frecM[4] += 1
                    if  manzana['p_lluvia_i'] > intervalo * 4.0:
                        frecM[5] += 1
               
                matrizR.append(frecM)

print matrizR
with open(join(scenariosFolder,"output_i.csv"), "wb") as f:
    writer = csv.writer(f)
    writer.writerows(matrizR)
