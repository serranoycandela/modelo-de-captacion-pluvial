from os import listdir
from os.path import join, isdir
import numpy as np
import csv

scenariosFolder = "Users/fidel/Dropbox/EscenariosMarzo"

matrizR = []#una lista vacia, que sera una lista de listas mas adelante
matrizR.append(["escenario", "masde50"])#el header del csv
maximo = 0
minimo = 200
for carpeta in sorted(listdir(scenariosFolder)):#lista los archivos y carpetas de scenariosFolder
    carpetaPath = join(scenariosFolder, carpeta)#construye el path
    if isdir(carpetaPath) and carpeta.startswith("escenario"):#si es carpeta de escenario
        for shapefile in listdir(carpetaPath): #solo para recorrer los archivos y encontrar el shp
            
            if shapefile.endswith(".shp"):#si es el .shp
                print carpeta, shapefile
                shape_path = join(carpetaPath, shapefile) #crea el path al shape
                manzana_layer = QgsVectorLayer(shape_path, "manzanas", "ogr") #crea una capa de qgis para ese path
                frecM = [carpeta,0] #este es una lista para rellenar con la cuenta de las manzanas que pasan de 50
                intervalo = 17.6
                for manzana in manzana_layer.getFeatures():# para cada manzana...
             
                    if  manzana['p_lluvia_i'] >= 50:
                        frecM[1] += 1 #agregale uno a la cuenta
              
                matrizR.append(frecM)# agrega este escenario (un renglon) a la matriz que sera el csv 

with open(join(scenariosFolder,"masde50_i.csv"), "wb") as f: #escribe el csv
    writer = csv.writer(f)
    writer.writerows(matrizR)
