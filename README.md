# Modelo de captación pluvial

Es un modelo de diferencias que corre a nivel manzana para todas las manzanas de la CDMX, para simular la eficacia de los sistemas de captación pluvial, utilizando los datos de lluvia diaria inferidos a partir del radar meteorológico Cerro Catedral.



<img src="efecto_lluvia.png" width="400px">

