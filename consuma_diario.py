import json
from os.path import join, isdir, exists
from os import listdir

carpeta_exterior = "/Users/fidel/Dropbox (LANCIS)/EscenariosMarzo"
for este_folder in listdir(carpeta_exterior):
    folder = join(carpeta_exterior, este_folder)
    if isdir(folder):
        #folder = "/Users/fidel/Dropbox (LANCIS)/EscenariosMarzo/escenario01"
        if exists(join(folder,'sum_used_array.json')):
            data = json.load(open(join(folder,'sum_used_array.json')))
            dato_anterior = 0
            array_consumo = []
            for dato in data:
                consumo = dato - dato_anterior
                print dato, consumo
                array_consumo.append(consumo)
                dato_anterior = dato

            with open(join(folder,'consumo_array.json'), 'w') as outfile:
                json.dump(array_consumo, outfile)
